<?php

/*
 * Template Name: Homepage Template
 */

$banner = get_field('banner');
$categories = get_field('categories');
$details = get_field('details');

get_header() ?>

    <section class="dbk-body-wrapper">
        <div id="dbk-main-container" data-dbk-home-page data-dbk-landing-page="container">

            <?php if (!empty($banner)) : ?>
                <div class="bij-herobanner">
                <div class="dbk-banner dbk-banner_bij dbk-banner_brand h-auto dbk-banner_margin"
                     style="background-color: transparent" data-tm-campaign="true" data-tm-component-id="hero_banner"
                     data-dbk-hoverstate data-tm-grouped-promotions>
                    <span class="d-none" data-tm-campaign-item-label="home-header-sale-wk16"></span>
                    <div class="dbk-imageholder" data-dbk-hoverstate-target="1">
                        <div class="dbk-banner--image  ">
                            <picture>
                                <img src="<?php echo $banner['banner_bg_image'] ?>"
                                     data-tm-campaign-item-img="<?php echo $banner['banner_bg_image'] ?>"/>
                            </picture>
                        </div>
                    </div>
                    <div class="container pos-relative py-1">
                        <div class="dbk-banner--text   ">

                            <header>
                                <h1>
                                    <span class="h5 d-block mb-1"><?php echo $banner['banner_small_text'] ?></span>
                                    <span class="headings-sale"><?php echo $banner['banner_title'] ?></span>
                                </h1>
                            </header>
                            <span class="dbk-banner--description d-block w-100">
                                <?php echo $banner['banner_subtitle'] ?>
                                <span class="d-none d-sm-block w-100"></span>
                            </span>
                            <a target="<?php echo $banner['banner_button_url']['target'] ?>" class="btn btn--primary-inverse" href="<?php echo $banner['banner_button_url']['url'] ?>" data-tm-campaign-item-href="<?php echo $banner['banner_button_url']['url'] ?>" data-dbk-hoverstate-target="1">
                                <?php echo $banner['banner_button_url']['title'] ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <?php if (!empty($categories)) : ?>
                <div class="visual-navigation-4">
                    <div class="dbk-visual-navigation dbk-visual-navigation--four " data-dbk-hoverstate
                         data-tm-campaign="true" data-tm-component-id="discover_new_categories">
                        <span class="hidden" data-tm-campaign-item-label="discover_new_categories"></span>

                        <?php foreach ($categories as $category) : ?>
                            <article class="dbk-visual-navigation-item">
                                <span class="hidden" data-tm-campaign-item-label="home-visnav-dames-wk12"></span>
                                <a href="<?php echo $category['categories_link']['url'] ?>" title="<?php echo $category['categories_link']['title'] ?>" data-tm-campaign-item-href="<?php echo $category['categories_link']['url'] ?>">
                                    <div class="dbk-imageholder" data-dbk-hoverstate-target="1">
                                        <picture>
                                            <img src="<?php echo $category['categories_background_image']['url'] ?>"
                                                 alt="<?php echo $category['categories_background_image']['alt'] ?>"
                                                 data-tm-campaign-item-img="<?php echo $category['categories_background_image']['url'] ?>"/>
                                        </picture>
                                        <header class="dbk-visual-navigation-item--text p-2">
                                            <p class="h2 headings-1 t-white m-0">
                                                <?php echo $category['categories_link']['title'] ?>
                                            </p>
                                        </header>
                                    </div>
                                </a>
                            </article>
                        <?php endforeach; ?>

                    </div>
                </div>
            <?php endif; ?>
        </div>
    </section>

    <?php if (!empty($details)) : ?>
        <div class="dbk-footer--usp container mt-3 mb-3">
            <ul class="list-inline d-flex mb-0 flex-wrap">

                <?php foreach($details as $item) : ?>
                    <li class="t-bold d-flex col-12 col-sm-6 col-md-4">
                        <span class="icon icon--sm">
                            <svg title="icon--check" role="img">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon--check"></use>
                            </svg>
                        </span>
                        <a href="#"><span><?php echo $item['details_text'] ?></span></a>
                    </li>
                <?php endforeach; ?>

            </ul>
        </div>
    <?php endif; ?>


<?php get_footer(); ?>