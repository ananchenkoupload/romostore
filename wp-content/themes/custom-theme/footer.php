<?php

$footer__social_links = get_field('footer__social_links', 'options');

?>

<footer class="dbk-footer" data-dbk-test>
    <div class="container">
        <div class="col-sm-12">

            <nav class="accordion pt-5" data-dbk-accordion>
                <div class="row">
                    <div class="col-12 col-md-3">
                        <div class="accordion__item" data-dbk-accordion-item>
                            <div class="accordion__header" data-dbk-accordion-header>
                                <a href="<?php echo site_url() ?>" class="footer_logo">
                                    <img src="<?php the_field('footer_logo', 'options'); ?>" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="accordion__item" data-dbk-accordion-item>

                            <?php wp_nav_menu( array(
                                    'theme_location' => 'footer-categories-menu',
                                    'container' =>'',
                                    'menu_class' => 'accordion__list list-unstyled'
                                )
                            ) ?>

                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="accordion__item" data-dbk-accordion-item>

                            <?php wp_nav_menu( array(
                                    'theme_location' => 'footer-pages-menu',
                                    'container' =>'',
                                    'menu_class' => 'accordion__list list-unstyled'
                                )
                            ) ?>

                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="accordion__item" data-dbk-accordion-item>
                            <div class="accordion__header" data-dbk-accordion-header>
                                <span class="t-none">Sign Up to Our Newsletter</span>
                            </div>

                            <?php echo do_shortcode('[wpforms id="36" title="false" description="false"]') ?>

                            <div class="accordion__header" data-dbk-accordion-header>
                                <span class="t-none">Follow Us</span>
                            </div>

                            <ul class="parsys dbk-footer--list_social mb-3 t-md-left list-inline">

                                <?php if (!empty($footer__social_links['footer__social_links_instagram'])) : ?>
                                    <li class="list-inline-item mr-0">
                                        <a href="<?php echo $footer__social_links['footer__social_links_instagram'] ?>" target="_blank" rel="nofollow" title="Instagram">
                                            <span class="icon icon--lg">
                                                <svg title="icon--social-instagram" role="img">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon--social-instagram"></use>
                                                </svg>
                                            </span>
                                        </a>
                                    </li>
                                <?php endif; ?>

                                <?php if (!empty($footer__social_links['footer__social_links_facebook'])) : ?>
                                    <li class="list-inline-item mr-0">
                                        <a href="<?php echo $footer__social_links['footer__social_links_facebook'] ?>" target="_blank" rel="nofollow" title="Facebook">
                                            <span class="icon icon--lg">
                                                <svg title="icon--social-facebook" role="img">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon--social-facebook"></use>
                                                </svg>
                                            </span>
                                        </a>
                                    </li>
                                <?php endif; ?>

                                <?php if (!empty($footer__social_links['footer__social_links_youtube'])) : ?>
                                    <li class="list-inline-item mr-0">
                                        <a href="<?php echo $footer__social_links['footer__social_links_youtube'] ?>" target="_blank" rel="nofollow" title="Youtube">
                                            <span class="icon icon--lg">
                                                <svg title="icon--social-youtube" role="img">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon--social-youtube"></use>
                                                </svg>
                                            </span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <div class="container" data-dbk-language="footer-language-switch"></div>
</footer>
<div class="newpar new section">
</div>
<div class="par iparys_inherited">
</div>
</div>

<script>
    !function (e) {
        function r(r) {
            for (var d, t, o = r[0], n = r[1], f = r[2], b = 0, l = []; b < o.length; b++) t = o[b], Object.prototype.hasOwnProperty.call(c, t) && c[t] && l.push(c[t][0]), c[t] = 0;
            for (d in n) Object.prototype.hasOwnProperty.call(n, d) && (e[d] = n[d]);
            for (g && g(r); l.length;) l.shift()();
            return i.push.apply(i, f || []), a()
        }

        function a() {
            for (var e, r = 0; r < i.length; r++) {
                for (var a = i[r], d = !0, t = 1; t < a.length; t++) {
                    var n = a[t];
                    0 !== c[n] && (d = !1)
                }
                d && (i.splice(r--, 1), e = o(o.s = a[0]))
            }
            return e
        }

        var d = {}, t = {"dbk.bundle.manifest": 0}, c = {"dbk.bundle.manifest": 0}, i = [];

        function o(r) {
            if (d[r]) return d[r].exports;
            var a = d[r] = {i: r, l: !1, exports: {}};
            return e[r].call(a.exports, a, a.exports, o), a.l = !0, a.exports
        }

        o.e = function (e) {
            var r = [];
            t[e] ? r.push(t[e]) : 0 !== t[e] && {
                CookieBar: 1,
                GiftFinder: 1
            }[e] && r.push(t[e] = new Promise((function (r, a) {
                for (var d = "clientlib-dbk/css/" + ({
                    Anniversary: "Anniversary",
                    BlogCategoryOverviewPage: "BlogCategoryOverviewPage",
                    BlogOverviewPage: "BlogOverviewPage",
                    BlogPage: "BlogPage",
                    BrandTriple: "BrandTriple",
                    CookieBar: "CookieBar",
                    CreditCardApplication: "CreditCardApplication",
                    FooterLanguageSwitch: "FooterLanguageSwitch",
                    GiftCard: "GiftCard",
                    GiftFinder: "GiftFinder",
                    LandingPage: "LandingPage",
                    MemberActivation: "MemberActivation",
                    ProductCarouselWithCampaignTracking: "ProductCarouselWithCampaignTracking",
                    ProductDetailPage: "ProductDetailPage",
                    ProductListerPage: "ProductListerPage",
                    Styling: "Styling",
                    StylingSignup: "StylingSignup",
                    Toplist: "Toplist",
                    TrendsOverview: "TrendsOverview",
                    Triple: "Triple",
                    VisitorCounter: "VisitorCounter",
                    "dbk.form.custom.cardtracker": "dbk.form.custom.cardtracker",
                    "dbk.form.custom.contact": "dbk.form.custom.contact",
                    "dbk.form.custom.giftcard": "dbk.form.custom.giftcard",
                    "dbk.pages.brand.overview": "dbk.pages.brand.overview",
                    personalLister: "personalLister"
                }[e] || e) + "-chunk." + {
                    0: "31d6cfe0d16ae931b73c",
                    1: "31d6cfe0d16ae931b73c",
                    2: "31d6cfe0d16ae931b73c",
                    3: "31d6cfe0d16ae931b73c",
                    4: "31d6cfe0d16ae931b73c",
                    5: "31d6cfe0d16ae931b73c",
                    6: "31d6cfe0d16ae931b73c",
                    7: "31d6cfe0d16ae931b73c",
                    8: "31d6cfe0d16ae931b73c",
                    9: "31d6cfe0d16ae931b73c",
                    10: "31d6cfe0d16ae931b73c",
                    11: "31d6cfe0d16ae931b73c",
                    12: "31d6cfe0d16ae931b73c",
                    13: "31d6cfe0d16ae931b73c",
                    14: "31d6cfe0d16ae931b73c",
                    15: "31d6cfe0d16ae931b73c",
                    16: "31d6cfe0d16ae931b73c",
                    17: "31d6cfe0d16ae931b73c",
                    18: "31d6cfe0d16ae931b73c",
                    19: "31d6cfe0d16ae931b73c",
                    20: "31d6cfe0d16ae931b73c",
                    Anniversary: "31d6cfe0d16ae931b73c",
                    BlogCategoryOverviewPage: "31d6cfe0d16ae931b73c",
                    BlogOverviewPage: "31d6cfe0d16ae931b73c",
                    BlogPage: "31d6cfe0d16ae931b73c",
                    BrandTriple: "31d6cfe0d16ae931b73c",
                    CookieBar: "4806c8bfa4089f542b56",
                    CreditCardApplication: "31d6cfe0d16ae931b73c",
                    FooterLanguageSwitch: "31d6cfe0d16ae931b73c",
                    GiftCard: "31d6cfe0d16ae931b73c",
                    GiftFinder: "8dc7d5ce86215608dddf",
                    LandingPage: "31d6cfe0d16ae931b73c",
                    MemberActivation: "31d6cfe0d16ae931b73c",
                    ProductCarouselWithCampaignTracking: "31d6cfe0d16ae931b73c",
                    ProductDetailPage: "31d6cfe0d16ae931b73c",
                    ProductListerPage: "31d6cfe0d16ae931b73c",
                    Styling: "31d6cfe0d16ae931b73c",
                    StylingSignup: "31d6cfe0d16ae931b73c",
                    Toplist: "31d6cfe0d16ae931b73c",
                    TrendsOverview: "31d6cfe0d16ae931b73c",
                    Triple: "31d6cfe0d16ae931b73c",
                    VisitorCounter: "31d6cfe0d16ae931b73c",
                    "dbk.form.custom.cardtracker": "31d6cfe0d16ae931b73c",
                    "dbk.form.custom.contact": "31d6cfe0d16ae931b73c",
                    "dbk.form.custom.giftcard": "31d6cfe0d16ae931b73c",
                    "dbk.pages.brand.overview": "31d6cfe0d16ae931b73c",
                    personalLister: "31d6cfe0d16ae931b73c"
                }[e] + ".css", c = o.p + d, i = document.getElementsByTagName("link"), n = 0; n < i.length; n++) {
                    var f = (g = i[n]).getAttribute("data-href") || g.getAttribute("href");
                    if ("stylesheet" === g.rel && (f === d || f === c)) return r()
                }
                var b = document.getElementsByTagName("style");
                for (n = 0; n < b.length; n++) {
                    var g;
                    if ((f = (g = b[n]).getAttribute("data-href")) === d || f === c) return r()
                }
                var l = document.createElement("link");
                l.rel = "stylesheet", l.type = "text/css", l.onload = r, l.onerror = function (r) {
                    var d = r && r.target && r.target.src || c,
                        i = new Error("Loading CSS chunk " + e + " failed.\n(" + d + ")");
                    i.code = "CSS_CHUNK_LOAD_FAILED", i.request = d, delete t[e], l.parentNode.removeChild(l), a(i)
                }, l.href = c, document.getElementsByTagName("head")[0].appendChild(l)
            })).then((function () {
                t[e] = 0
            })));
            var a = c[e];
            if (0 !== a) if (a) r.push(a[2]); else {
                var d = new Promise((function (r, d) {
                    a = c[e] = [r, d]
                }));
                r.push(a[2] = d);
                var i, n = document.createElement("script");
                n.charset = "utf-8", n.timeout = 120, o.nc && n.setAttribute("nonce", o.nc), n.src = function (e) {
                    return o.p + "clientlib-dbk/js/" + ({
                        Anniversary: "Anniversary",
                        BlogCategoryOverviewPage: "BlogCategoryOverviewPage",
                        BlogOverviewPage: "BlogOverviewPage",
                        BlogPage: "BlogPage",
                        BrandTriple: "BrandTriple",
                        CookieBar: "CookieBar",
                        CreditCardApplication: "CreditCardApplication",
                        FooterLanguageSwitch: "FooterLanguageSwitch",
                        GiftCard: "GiftCard",
                        GiftFinder: "GiftFinder",
                        LandingPage: "LandingPage",
                        MemberActivation: "MemberActivation",
                        ProductCarouselWithCampaignTracking: "ProductCarouselWithCampaignTracking",
                        ProductDetailPage: "ProductDetailPage",
                        ProductListerPage: "ProductListerPage",
                        Styling: "Styling",
                        StylingSignup: "StylingSignup",
                        Toplist: "Toplist",
                        TrendsOverview: "TrendsOverview",
                        Triple: "Triple",
                        VisitorCounter: "VisitorCounter",
                        "dbk.form.custom.cardtracker": "dbk.form.custom.cardtracker",
                        "dbk.form.custom.contact": "dbk.form.custom.contact",
                        "dbk.form.custom.giftcard": "dbk.form.custom.giftcard",
                        "dbk.pages.brand.overview": "dbk.pages.brand.overview",
                        personalLister: "personalLister"
                    }[e] || e) + "-chunk." + {
                        0: "d09104d2d1e16240b244",
                        1: "6e1e1fd57741c6e2a819",
                        2: "f80024b5fb6f50aac1ae",
                        3: "4b0ed1551fb0d7d1549c",
                        4: "7db595f0a6789fbb946a",
                        5: "e50617576ba8825390d9",
                        6: "2d65634952541d5b4db8",
                        7: "ddb0d5bf7c74c80587da",
                        8: "e47a3f203da42f5fe7bb",
                        9: "9cb4901e0926e7676dfc",
                        10: "e62499c712d882b18a90",
                        11: "3083d8b9e50dd7d3c9d9",
                        12: "aedd9c2aa849693ff1e1",
                        13: "046f362499dd36850c66",
                        14: "6b91b031f8c759b1a087",
                        15: "46c21f5aa1ba442bff94",
                        16: "8b5f1deb20dba870d554",
                        17: "f39b316fd631495db1f6",
                        18: "b7a3c7bd41f819d1dc24",
                        19: "6bc3e4e20b75b373c131",
                        20: "b9678f2aac2b52ccc611",
                        Anniversary: "0843b12095593a207bd2",
                        BlogCategoryOverviewPage: "c14a3d5a87d990f5fb50",
                        BlogOverviewPage: "a5c65f5b3edde413c280",
                        BlogPage: "e054b75b296d6c64abdc",
                        BrandTriple: "bc81c1dace66031027ea",
                        CookieBar: "a6da49e30330432b0a65",
                        CreditCardApplication: "53206a2d5ee04dd0303e",
                        FooterLanguageSwitch: "b71227dffc4df9706b2e",
                        GiftCard: "587fef74690ab2b122bd",
                        GiftFinder: "e6dbb35e6265dc342846",
                        LandingPage: "f127ab35172852f95e84",
                        MemberActivation: "9d4a48e3966f84d06b8d",
                        ProductCarouselWithCampaignTracking: "7d944e64fed3633da88a",
                        ProductDetailPage: "56c2dd4e2ec5d7a5ddaa",
                        ProductListerPage: "cb5c3d2882f4d3b4fdb8",
                        Styling: "105066916e9c0dd434b5",
                        StylingSignup: "0b2a05f075a8ae93f471",
                        Toplist: "77c9813483d18144cac5",
                        TrendsOverview: "d566f6d26c4a08d89dd6",
                        Triple: "cfe9d7306e5954924c8b",
                        VisitorCounter: "f43c6a94ee485c288a53",
                        "dbk.form.custom.cardtracker": "4f5993b71df3f1f730e0",
                        "dbk.form.custom.contact": "a141dc735fabef69ad2a",
                        "dbk.form.custom.giftcard": "29d9b48298fb3f2da5fc",
                        "dbk.pages.brand.overview": "acf204c11c09cee6212b",
                        personalLister: "556d5bc01fb606c49d61"
                    }[e] + ".js"
                }(e);
                var f = new Error;
                i = function (r) {
                    n.onerror = n.onload = null, clearTimeout(b);
                    var a = c[e];
                    if (0 !== a) {
                        if (a) {
                            var d = r && ("load" === r.type ? "missing" : r.type), t = r && r.target && r.target.src;
                            f.message = "Loading chunk " + e + " failed.\n(" + d + ": " + t + ")", f.name = "ChunkLoadError", f.type = d, f.request = t, a[1](f)
                        }
                        c[e] = void 0
                    }
                };
                var b = setTimeout((function () {
                    i({type: "timeout", target: n})
                }), 12e4);
                n.onerror = n.onload = i, document.head.appendChild(n)
            }
            return Promise.all(r)
        }, o.m = e, o.c = d, o.d = function (e, r, a) {
            o.o(e, r) || Object.defineProperty(e, r, {enumerable: !0, get: a})
        }, o.r = function (e) {
            "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {value: "Module"}), Object.defineProperty(e, "__esModule", {value: !0})
        }, o.t = function (e, r) {
            if (1 & r && (e = o(e)), 8 & r) return e;
            if (4 & r && "object" == typeof e && e && e.__esModule) return e;
            var a = Object.create(null);
            if (o.r(a), Object.defineProperty(a, "default", {
                enumerable: !0,
                value: e
            }), 2 & r && "string" != typeof e) for (var d in e) o.d(a, d, function (r) {
                return e[r]
            }.bind(null, d));
            return a
        }, o.n = function (e) {
            var r = e && e.__esModule ? function () {
                return e.default
            } : function () {
                return e
            };
            return o.d(r, "a", r), r
        }, o.o = function (e, r) {
            return Object.prototype.hasOwnProperty.call(e, r)
        }, o.p = "/etc/designs/debijenkorf-website/responsive-assets/bundles/", o.oe = function (e) {
            throw console.error(e), e
        };
        var n = window.webpackJsonp = window.webpackJsonp || [], f = n.push.bind(n);
        n.push = r, n = n.slice();
        for (var b = 0; b < n.length; b++) r(n[b]);
        var g = f;
        a()
    }([]);
</script>

<div class="dbk-ocp">
    <div data-at="dbk-ocp-nav" data-dbk-off-canvas-pane="dbk-ocp-nav" class="ocp ocp--left">
        <div role="presentation" class="ocp__backdrop"></div>
        <div class="ocp__content dbk-ocp-nav">
            <header class="ocp__header mx-0 px-4">
                <button type="button" class="btn ocp__header__close-button btn--naked" data-at="close-ocp"><span
                            class="icon icon--md"><svg role="img"><title>close</title><use
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    xlink:href="#icon--close"></use></svg></span></button>
                <h1 class="ocp__header__text">
                    <span>
                        <a class="dbk-header--logo logo" href="<?php echo site_url() ?>">
                            <img src="<?php the_field('header_logo', 'options'); ?>" alt="">
                        </a>
                    </span>
                </h1></header>
            <div class="ocp__body p-0">
                <section class="overlay-search" data-dbk-search="true">
                    <div class="dbk-search closed ">
                        <div class="isOverlay  disableAnimation">
                            <button class="dbk-search--mobile-button btn btn--naked t-black"><span
                                        class="icon icon--lg"><svg role="img"><title>search</title><use
                                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                                xlink:href="#icon--search"></use></svg></span></button>
                            <form class="dbk-search--form closed notranslate" data-at="SearchForm"
                                  action="">
                                <div class="pos-relative"><input class="dbk-form--field form-control" type="search"
                                                                 placeholder="Search..."
                                                                 name="SearchTerm" autocomplete="off" value="">
                                    <button aria-label="zoeken" data-at="SearchSubmit" class="dbk-form--submit"
                                            type="submit"><span class="icon icon--sm"><svg role="img"><title>icon--search</title><use
                                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                                        xlink:href="#icon--search"></use></svg></span></button>
                                </div>
                            </form>
                        </div>
                        <div class="search-overlay  disableAnimation"></div>
                    </div>
                </section>
                <div class="d-lg-none pb-4">
                    <nav class="dbk-hubspoke-nav">

                        <?php wp_nav_menu( array(
                                'theme_location' => 'header-menu',
                                'container' => '',
                                'menu_class' => 'list-unstyled py-3 mb-0 bg-creme bb slide--right',
                                'walker' => new Header_Mobile_Walker_Nav_Menu()
                            )
                        ) ?>

                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<?php wp_footer(); ?>
</body>
</html>
