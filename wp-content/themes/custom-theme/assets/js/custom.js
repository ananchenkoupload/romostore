jQuery(document).ready(function ($) {
    $('.dbk-header--menu-toggle, .ocp__header__close-button').on('click', function () {
        $('body').toggleClass('scroll-lock');

        $('.ocp.ocp--left').toggleClass('ocp--open');
    });
});